package com.rewardo.foreachsols.Application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

public class RewardoApplication extends MultiDexApplication {

    public static final String TAG = "Rewardo";
    private static RewardoApplication rewardoApplication;

    @Override
    public void onCreate(){

        super.onCreate();
        rewardoApplication=this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public static synchronized RewardoApplication getInstance(){

        return rewardoApplication;
    }
}