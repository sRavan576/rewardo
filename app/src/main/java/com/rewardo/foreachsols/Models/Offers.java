package com.rewardo.foreachsols.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Offers {

    @SerializedName("data")
    @Expose
    private ArrayList<OfferItem> Data;

    @SerializedName("message")
    @Expose
    private String Message;

    @SerializedName("itemsCount")
    @Expose
    private Integer ItemsCount;

    @SerializedName("loadMoreUrl")
    @Expose
    private String LoadMoreUrl;

    public ArrayList<OfferItem> getData() {
        return Data;
    }

    public void setData(ArrayList<OfferItem> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Integer getItemsCount() {
        return ItemsCount;
    }

    public void setItemsCount(Integer itemsCount) {
        ItemsCount = itemsCount;
    }

    public String getLoadMoreUrl() {
        return LoadMoreUrl;
    }

    public void setLoadMoreUrl(String loadMoreUrl) {
        LoadMoreUrl = loadMoreUrl;
    }
}
