package com.rewardo.foreachsols.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StoresByCategory {

    @SerializedName("data")
    @Expose
    private ArrayList<StoreItemByCategory> Data;

    @SerializedName("message")
    @Expose
    private String Message;

    @SerializedName("itemsCount")
    @Expose
    private Integer ItemsCount;

    @SerializedName("loadMoreUrl")
    @Expose
    private String LoadMoreUrl;

    public ArrayList<StoreItemByCategory> getData() {
        return Data;
    }

    public void setData(ArrayList<StoreItemByCategory> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Integer getItemsCount() {
        return ItemsCount;
    }

    public void setItemsCount(Integer itemsCount) {
        ItemsCount = itemsCount;
    }

    public String getLoadMoreUrl() {
        return LoadMoreUrl;
    }

    public void setLoadMoreUrl(String loadMoreUrl) {
        LoadMoreUrl = loadMoreUrl;
    }
}

