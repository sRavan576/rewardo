package com.rewardo.foreachsols.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreItemByCategory {

    @SerializedName("id")
    @Expose
    private Integer Id;

    @SerializedName("name")
    @Expose
    private String Name;

    @SerializedName("url")
    @Expose
    private String Url;

    @SerializedName("imageUrl")
    @Expose
    private String ImageUrl;

    @SerializedName("totalCards")
    @Expose
    private String TotalCards;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getTotalCards() {
        return TotalCards;
    }

    public void setTotalCards(String totalCards) {
        TotalCards = totalCards;
    }


}
