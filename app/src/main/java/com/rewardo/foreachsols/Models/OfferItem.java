package com.rewardo.foreachsols.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferItem {

    @SerializedName("id")
    @Expose
    private Integer Id;

    @SerializedName("title")
    @Expose
    private String Title;

    @SerializedName("details")
    @Expose
    private String Details;

    @SerializedName("imageUrl")
    @Expose
    private String ImageUrl;

    @SerializedName("coupon")
    @Expose
    private String Coupon;

    @SerializedName("offerUrl")
    @Expose
    private String OfferUrl;

    @SerializedName("endsIn")
    @Expose
    private String EndsIn;


    @SerializedName("hotDealScale")
    @Expose
    private Integer HotDealScale;

    @SerializedName("vendorName")
    @Expose
    private String VendorName;

    @SerializedName("vendorImageUrl")
    @Expose
    private String VendorImageUrl;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getCoupon() {
        return Coupon;
    }

    public void setCoupon(String coupon) {
        Coupon = coupon;
    }

    public String getOfferUrl() {
        return OfferUrl;
    }

    public void setOfferUrl(String offerUrl) {
        OfferUrl = offerUrl;
    }

    public String getEndsIn() {
        return EndsIn;
    }

    public void setEndsIn(String endsIn) {
        EndsIn = endsIn;
    }

    public Integer getHotDealScale() {
        return HotDealScale;
    }

    public void setHotDealScale(Integer hotDealScale) {
        HotDealScale = hotDealScale;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }

    public String getVendorImageUrl() {
        return VendorImageUrl;
    }

    public void setVendorImageUrl(String vendorImageUrl) {
        VendorImageUrl = vendorImageUrl;
    }
}
