package com.rewardo.foreachsols.Interfaces;

import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.Models.StoreItemByCategory;
import com.rewardo.foreachsols.Models.StoresByCategory;

public interface IStoresByCategoryAdapter {

    public void buttonClick(StoreItemByCategory storeItemByCategory);

}
