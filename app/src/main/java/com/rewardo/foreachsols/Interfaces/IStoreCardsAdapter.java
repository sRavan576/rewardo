package com.rewardo.foreachsols.Interfaces;


import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.StoreItem;

public interface IStoreCardsAdapter {

    public void itemClick(StoreItem storeItem, int position);

    public void buttonClick(StoreItem storeItem);



}
