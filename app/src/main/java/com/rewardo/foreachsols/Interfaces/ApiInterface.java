package com.rewardo.foreachsols.Interfaces;

import com.rewardo.foreachsols.Models.Categories;
import com.rewardo.foreachsols.Models.Offers;
import com.rewardo.foreachsols.Models.StoreCards;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.Models.StoresByCategory;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

//    @GET("api/store/0/1")
//    Call<StoreCards> GetAllOffers();

    @GET("{Url}")
    Call<StoreCards> GetTopStores(@Path("Url") String LodeMoreUrl);

    @GET("api/category/all")
    Call<Categories> GetAllCategories();

    @GET("{Url}")
    Call<Offers> GetAllOffers(@Path("Url") String LodeMoreUrl);

    @GET("api/store/{CategoryId}/1")
    Call<StoresByCategory> GetAllStoresByCategories(@Path("CategoryId") int CategoryId);

    @GET("{Url}")
    Call<Offers> GetOffersByStore(@Path("Url") String LodeMoreUrl);

}
