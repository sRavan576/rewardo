package com.rewardo.foreachsols.Interfaces;

import com.rewardo.foreachsols.Models.CategoryItem;

public interface ICategoriesAdapter {
    public void itemClick(CategoryItem categoryItem, int position);
}
