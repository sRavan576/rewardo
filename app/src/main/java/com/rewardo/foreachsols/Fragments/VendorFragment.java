package com.rewardo.foreachsols.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rewardo.foreachsols.Activities.HelperActivity;
import com.rewardo.foreachsols.Activities.WebViewActivity;
import com.rewardo.foreachsols.Adapters.OffersAdapter;
import com.rewardo.foreachsols.Adapters.OffersByStoresAdapter;
import com.rewardo.foreachsols.ApiClient;
import com.rewardo.foreachsols.Constants.Constants;
import com.rewardo.foreachsols.Interfaces.ApiInterface;
import com.rewardo.foreachsols.Interfaces.IOffersByStoresAdapter;
import com.rewardo.foreachsols.Interfaces.IStoreCardsAdapter;
import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.Models.OfferItem;
import com.rewardo.foreachsols.Models.Offers;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;


public class VendorFragment extends Fragment implements IOffersByStoresAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;

    TextView tv_reward;
    ImageView iv_vendorLogo, ivRewardIcon;

    BottomNavigationView bottomNavigationView;

    Toolbar toolbar;
    private RecyclerView offersByStoresRecyclerView;
    private RecyclerView.Adapter adapter;
    LinearLayoutManager manager;
    public ArrayList<OfferItem> offersList = new ArrayList<OfferItem>();
    public Offers offers = new Offers();
    public OfferItem offerItem = new OfferItem();
    ToolbarInterface toolbarCallback;
    ProgressBar progressBar;
    Boolean isScrolling = false;
    String lodeMoreUrl =  "api/store/offers/%VendorId%/1";
    String previousLodeMoreUrl = "api/store/offers/1/1";
    int currentItems,scrollOutItems,totalItems;
    public IOffersByStoresAdapter iOffersByStoresAdapter;
    public OffersByStoresAdapter offersByStoresAdapter;

    private OnFragmentInteractionListener mListener;

    public VendorFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VendorFragment newInstance(String param1, String param2) {
        VendorFragment fragment = new VendorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static VendorFragment newInstance(String param1, String param2, String param3,String param4) {
        VendorFragment fragment = new VendorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3,param3);
        args.putString(ARG_PARAM4,param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
        if(mParam1 != null){
            lodeMoreUrl = lodeMoreUrl.replace("%VendorId%",mParam1);
            previousLodeMoreUrl = lodeMoreUrl;
        }
        else{
            lodeMoreUrl = lodeMoreUrl.replace("%VendorId%","0");
            previousLodeMoreUrl = lodeMoreUrl;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vendor, container, false);

        toolbarCallback = (ToolbarInterface) getActivity();
        ivRewardIcon = (ImageView)view.findViewById(R.id.rewardsIcon) ;
        ivRewardIcon.setImageResource(R.drawable.rewardicon);
        iv_vendorLogo=(ImageView)view.findViewById(R.id.vendorLogo);
        tv_reward=(TextView)view.findViewById(R.id.reward);
        toolbar = view.findViewById(R.id.toolbar);

        if (mParam2 != null){
            Picasso.get().load(mParam2).into(iv_vendorLogo);
        }
        if(mParam3 !=null){
            tv_reward.setText(mParam3);
        }
        if (mParam4 != null){
           toolbarCallback.getToolbarResources((mParam4),1);
        }


        manager = new LinearLayoutManager(getContext());
        offersList = new ArrayList<OfferItem>();
        iOffersByStoresAdapter = this;
        offersByStoresRecyclerView = (RecyclerView) view.findViewById(R.id.offersByStoresRecyclerView);
        offersByStoresRecyclerView.setHasFixedSize(true);
        offersByStoresRecyclerView.setLayoutManager(manager);
        offersByStoresRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();
                scrollOutItems = manager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems)){
                    isScrolling = false;
                    getOffersOfStore(lodeMoreUrl);
                }
            }
        });




        getOffersOfStore(lodeMoreUrl);

        return view;
    }

    private void getOffersOfStore(String url) {

        if(url != null) {
//            progressBar.setVisibility(View.VISIBLE);

            ApiInterface apiInterface = ApiClient.getClient(Constants.OFFERS_BASE_URL).create(ApiInterface.class);
            retrofit2.Call<Offers> call = apiInterface.GetOffersByStore(url);

            call.enqueue(new Callback<Offers>() {
                @Override
                public void onResponse(retrofit2.Call<Offers> call, Response<Offers> response) {
                    Log.e("Response_retro", response.toString());
                    offers = response.body();
                    for (int i = 0; i < offers.getData().size(); i++) {
                        offersList.add(offers.getData().get(i));
                    }
                    previousLodeMoreUrl = lodeMoreUrl;
                    lodeMoreUrl = offers.getLoadMoreUrl();
                    adapter = new OffersByStoresAdapter(offersList, getContext(), iOffersByStoresAdapter);
                    offersByStoresRecyclerView.setAdapter(adapter);
//                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(retrofit2.Call<Offers> call, Throwable t) {
                    Log.e("Error_retro", t.toString());
//                    progressBar.setVisibility(View.GONE);
                }
            });
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void cardClick(OfferItem item) {

        Intent intent = new Intent(getContext(),WebViewActivity.class);
        intent.putExtra("WEBSITE_ADDRESS",item.getOfferUrl());
        intent.putExtra("NAME",item.getVendorName());
        startActivity(intent);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
