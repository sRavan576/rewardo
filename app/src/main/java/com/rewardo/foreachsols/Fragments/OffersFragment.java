package com.rewardo.foreachsols.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import com.rewardo.foreachsols.Activities.WebViewActivity;
import com.rewardo.foreachsols.Adapters.OffersAdapter;
import com.rewardo.foreachsols.Adapters.OffersByStoresAdapter;
import com.rewardo.foreachsols.ApiClient;
import com.rewardo.foreachsols.Constants.Constants;
import com.rewardo.foreachsols.Interfaces.ApiInterface;
import com.rewardo.foreachsols.Interfaces.IOffersAdapter;
import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.Models.OfferItem;
import com.rewardo.foreachsols.Models.Offers;
import com.rewardo.foreachsols.R;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;


public class OffersFragment extends Fragment implements IOffersAdapter{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public RecyclerView recyclerView ;
    public RecyclerView.Adapter adapter;
    LinearLayoutManager manager;
    ProgressBar progressBar;
    Boolean isScrolling = false;
    String lodeMoreUrl =  "/api/offers/all/1";
    String previousLodeMoreUrl = "/api/offers/all/1";
    int currentItems,scrollOutItems,totalItems;
    public IOffersAdapter iOffersAdapter;
    ToolbarInterface toolbarCallback;
    private ArrayList<OfferItem> offerItems = new ArrayList<OfferItem>();

    public Offers offers = new Offers();


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public OffersFragment() {

    }

    public static OffersFragment newInstance(String param1, String param2) {
        OffersFragment fragment = new OffersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         toolbarCallback = (ToolbarInterface) getActivity();


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_offers, container, false);

        toolbarCallback.getToolbarResources("Offers",1);


        manager = new LinearLayoutManager(getContext());
        offerItems = new ArrayList<OfferItem>();
        recyclerView = (RecyclerView) view.findViewById(R.id.offersRecyclerView);
        progressBar=(ProgressBar) view.findViewById(R.id.progressBar);
        iOffersAdapter = this;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        adapter = new OffersAdapter(offerItems,getContext(),iOffersAdapter);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();
                scrollOutItems = manager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems)){
                    isScrolling = false;
                    getOffers(lodeMoreUrl);
                }
            }
        });


        getOffers(lodeMoreUrl);

        return view;
    }

    public void getOffers(String url) {

        if(url != null) {
            progressBar.setVisibility(View.VISIBLE);

            ApiInterface apiInterface = ApiClient.getClient(Constants.OFFERS_BASE_URL).create(ApiInterface.class);
            retrofit2.Call<Offers> call = apiInterface.GetAllOffers(url);

            call.enqueue(new Callback<Offers>() {
                @Override
                public void onResponse(retrofit2.Call<Offers> call, Response<Offers> response) {
                    Log.e("Response_retro", response.toString());
                    offers = response.body();
//                    for (int i = 0; i < offers.getData().size(); i++) {
//                        offerItems.add(offers.getData().get(i));
//                    }
                    previousLodeMoreUrl = lodeMoreUrl;
                    lodeMoreUrl = offers.getLoadMoreUrl();
                    ((OffersAdapter) adapter).updateItems(offers.getData());
                    adapter.notifyDataSetChanged();
//                    adapter = new OffersAdapter(offerItems, getContext(), iOffersAdapter);
//                    recyclerView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(retrofit2.Call<Offers> call, Throwable t) {
                    Log.e("Error_retro", t.toString());
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void cardClick(OfferItem item) {


        Intent intent = new Intent(getContext(),WebViewActivity.class);
        intent.putExtra("WEBSITE_ADDRESS",item.getOfferUrl());
        intent.putExtra("NAME",item.getVendorName());
        startActivity(intent);


    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
