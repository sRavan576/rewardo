package com.rewardo.foreachsols.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rewardo.foreachsols.Activities.WebViewActivity;
import com.rewardo.foreachsols.Adapters.StoresByCategoryAdapter;
import com.rewardo.foreachsols.ApiClient;
import com.rewardo.foreachsols.Constants.Constants;
import com.rewardo.foreachsols.Interfaces.ApiInterface;
import com.rewardo.foreachsols.Interfaces.ICategoriesAdapter;
import com.rewardo.foreachsols.Interfaces.IStoresByCategoryAdapter;
import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.StoreItemByCategory;
import com.rewardo.foreachsols.Models.StoresByCategory;
import com.rewardo.foreachsols.R;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;


public class CategoriesFragment extends Fragment implements IStoresByCategoryAdapter,ICategoriesAdapter {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    ToolbarInterface toolbarCallback;

    private RecyclerView storesByCategoryRecyclerView ;
    private RecyclerView.Adapter storesByCategoryAdapter;
    private RecyclerView.Adapter adapter;
    LinearLayoutManager manager;
    public ArrayList<StoreItemByCategory> storeByCategoryList = new ArrayList<StoreItemByCategory>();
    public StoreItemByCategory storeItemByCategory = new StoreItemByCategory();
    public IStoresByCategoryAdapter iStoresByCategoryAdapter;
    public ICategoriesAdapter iCategoriesAdapter;
    public StoresByCategory storesByCategory = new StoresByCategory();
    String loadMoreUrl = "null";
    public  int CategoryId = 0;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CategoriesFragment() {

    }


    public static CategoriesFragment newInstance(String param1, String param2) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbarCallback = (ToolbarInterface) getActivity();


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if (mParam1 != null){
            CategoryId = Integer.parseInt(mParam1);
        }
        if(mParam2 != null){

            toolbarCallback.getToolbarResources((mParam2),1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view = inflater.inflate(R.layout.fragment_categories, container, false);

//        toolbarCallback.getToolbarResources("Grocery",1);

        iStoresByCategoryAdapter = this;
        iCategoriesAdapter = this;


        storesByCategoryRecyclerView = (RecyclerView)view.findViewById(R.id.storesByCategoryrecyclerView);
        manager=new LinearLayoutManager(getContext());
        storesByCategoryRecyclerView.setLayoutManager(manager);
        getAllStoresByCategories(CategoryId);
        return view;
    }

    private void getAllStoresByCategories(int CategoryId){


        ApiInterface apiInterface = ApiClient.getClient(Constants.OFFERS_BASE_URL).create(ApiInterface.class);
        retrofit2.Call<StoresByCategory> call = apiInterface.GetAllStoresByCategories(CategoryId);

        call.enqueue(new Callback<StoresByCategory>() {
            @Override
            public void onResponse(retrofit2.Call<StoresByCategory> call, Response<StoresByCategory> response) {
                Log.e("Response_retro", response.toString());
                storesByCategory = response.body();
                storeByCategoryList = storesByCategory.getData();



                manager.setOrientation(LinearLayoutManager.VERTICAL);
                storesByCategoryAdapter = new StoresByCategoryAdapter(storeByCategoryList, getContext(),iStoresByCategoryAdapter);
                storesByCategoryRecyclerView.setAdapter(storesByCategoryAdapter);

                //  progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(retrofit2.Call<StoresByCategory> call, Throwable t) {
                Log.e("Error_retro", t.toString());
                //  progressBar.setVisibility(View.GONE);
            }
        });

    }





    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void itemClick(CategoryItem categoryItem, int position) {

    }

    @Override
    public void buttonClick(StoreItemByCategory storeItemByCategory) {

        Intent intent = new Intent(getContext(),WebViewActivity.class);
        intent.putExtra("WEBSITE_ADDRESS",storeItemByCategory.getUrl());
        intent.putExtra("NAME",storeItemByCategory.getName());
        startActivity(intent);


    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
