package com.rewardo.foreachsols.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.rewardo.foreachsols.Activities.HelperActivity;
import com.rewardo.foreachsols.Activities.MainActivity;
import com.rewardo.foreachsols.Activities.WebViewActivity;
import com.rewardo.foreachsols.Adapters.CategoriesAdapter;
import com.rewardo.foreachsols.Adapters.StoreCardsAdapter;
import com.rewardo.foreachsols.Adapters.ViewPageAdapter;
import com.rewardo.foreachsols.ApiClient;
import com.rewardo.foreachsols.Constants.Constants;
import com.rewardo.foreachsols.Interfaces.ApiInterface;
import com.rewardo.foreachsols.Interfaces.ICategoriesAdapter;
import com.rewardo.foreachsols.Interfaces.IStoreCardsAdapter;
import com.rewardo.foreachsols.Interfaces.IStoresByCategoryAdapter;
import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.Models.Categories;
import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.StoreCards;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.Models.StoreItemByCategory;
import com.rewardo.foreachsols.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements IStoreCardsAdapter,ICategoriesAdapter,IStoresByCategoryAdapter {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    private RecyclerView recyclerView,categoryRecyclerView ;
    private RecyclerView.Adapter adapter,categoryAdapter;
    LinearLayoutManager manager,categoryManager;
    ProgressBar progressBar;
    public ArrayList<StoreItem> storeList = new ArrayList<StoreItem>();
    public ArrayList<CategoryItem> categoriesList = new ArrayList<CategoryItem>();
    public StoreItem storeItem = new StoreItem();
    public IStoreCardsAdapter iStoreCardsAdapter;
    public ICategoriesAdapter iCategoriesAdapter;
    public StoreCards storeCards = new StoreCards();
    public Categories categories = new Categories();

    Boolean isScrolling = false;
    String lodeMoreUrl =  "api/store/0/1";
    String previousLodeMoreUrl = "api/store/0/1";
    int currentItems,scrollOutItems,totalItems;

    AppBarLayout appBarLayout;

    public int currentItemId = R.id.storesIcon;


    NestedScrollView nestedScrollView;
    ViewPager viewPager;
    LinearLayout sliderDots;
    public int dotCounts;
    public ImageView[] dots;
    ToolbarInterface toolbarCallback;


    private String mParam1;
    private String mParam2;
    private String mParam3;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {

    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static HomeFragment newInstance(String param1, String param2,String param3) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbarCallback = (ToolbarInterface) getActivity();

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

       // Implementing SwipeRefreshLayout
        final android.support.v4.widget.SwipeRefreshLayout swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        swipeRefreshLayout.setRefreshing(false);

                        Snackbar.make(getActivity().findViewById(R.id.frameId), "Page Refresh Succesfull",Snackbar.LENGTH_SHORT).show();
                    }
                }, 500);
            }
        });

        //Setting name for my ActionBar within fargment
        toolbarCallback.getToolbarResources("Rewardo",1);

        //image slider
       viewPager=(ViewPager)view.findViewById(R.id.viewPager);
       sliderDots=(LinearLayout)view.findViewById(R.id.sliderDots);

       //RecyclerView
        iStoreCardsAdapter = this;
        iCategoriesAdapter = this;
        nestedScrollView=(NestedScrollView)view.findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        categoryRecyclerView=(RecyclerView)view.findViewById(R.id.categoryRecyclerView);
        manager = new LinearLayoutManager(getContext());
        categoryManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        categoryRecyclerView.setLayoutManager(categoryManager);

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged()
            {
                View view = (View)nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    // api call to get data
                    getStores(lodeMoreUrl);
                }
            }
        });

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
//                    isScrolling = true;
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                currentItems = manager.getChildCount();
//                totalItems = manager.getItemCount();
//                scrollOutItems = manager.findFirstVisibleItemPosition();
//
//                if(isScrolling && (currentItems + scrollOutItems == totalItems)){
//                    isScrolling = false;
//                    getStores(lodeMoreUrl);
//                }
//            }
//        });

        getCategories();

        getStores(lodeMoreUrl);


        //ImageSlider with Dots
        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(getContext());
        viewPager.setAdapter(viewPageAdapter);
        dotCounts = viewPageAdapter.getCount();
        dots = new ImageView[dotCounts];

        for (int i = 0; i < dotCounts; i++) {
            dots[i] = new ImageView(getContext());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.inactivedot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDots.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.activedot));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotCounts; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.inactivedot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.activedot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new myTimerTask(), 4000, 4000);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getCategories() {

        ApiInterface apiInterface = ApiClient.getClient(Constants.OFFERS_BASE_URL).create(ApiInterface.class);
        retrofit2.Call<Categories> call = apiInterface.GetAllCategories();


        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(retrofit2.Call<Categories> call, Response<Categories> response) {
                Log.e("Response_retro", response.toString());
                categories = response.body();
                categoriesList = categories.getData();

                categoryManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                categoryAdapter = new CategoriesAdapter(categoriesList, getContext(),iCategoriesAdapter);
                categoryRecyclerView.setAdapter(categoryAdapter);

                //  progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(retrofit2.Call<Categories> call, Throwable t) {
                Log.e("Error_retro", t.toString());
                //  progressBar.setVisibility(View.GONE);
            }
        });

    }

    private void getStores(String url) {

     //   progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiInterface = ApiClient.getClient(Constants.OFFERS_BASE_URL).create(ApiInterface.class);
        retrofit2.Call<StoreCards> call = apiInterface.GetTopStores(url);

        call.enqueue(new Callback<StoreCards>() {
            @Override
            public void onResponse(retrofit2.Call<StoreCards> call, Response<StoreCards> response) {
                Log.e("Response_retro", response.toString());
                storeCards = new StoreCards();
                storeCards = response.body();
                for(int i = 0;i<storeCards.getData().size();i++){
                   storeList.add(storeCards.getData().get(i));
                }

                previousLodeMoreUrl = lodeMoreUrl;
                lodeMoreUrl = storeCards.getLoadMoreUrl();
                manager.setOrientation(LinearLayoutManager.VERTICAL);
                adapter = new StoreCardsAdapter(storeList, getContext(), iStoreCardsAdapter);
                recyclerView.setAdapter(adapter);

                //  progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(retrofit2.Call<StoreCards> call, Throwable t) {
                Log.e("Error_retro", t.toString());
              //  progressBar.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void itemClick(CategoryItem categoryItem, int position) {

        Intent intent = new Intent(getContext(),HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY,2);
        intent.putExtra("ITEMID",Integer.toString(categoryItem.getId()));
        intent.putExtra("NAME",categoryItem.getName());
        startActivity(intent);
//       getFragmentManager().beginTransaction().replace(R.id.frameId,CategoriesFragment.newInstance(Integer.toString(categoryItem.getId()),categoryItem.getName()),"CategoryFrag").commit();
    }

    @Override
    public void itemClick(StoreItem storeItem, int position) {

        Intent intent = new Intent(getContext(),HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY,1);
        intent.putExtra("ITEMID",Integer.toString(storeItem.getId()));
        intent.putExtra("IMAGEURL",storeItem.getImageUrl());
        intent.putExtra("TOTALCARDS",storeItem.getTotalCards());
        intent.putExtra("NAME",storeItem.getName());
        startActivity(intent);

//        getFragmentManager().beginTransaction().replace(R.id.frameId,VendorFragment.newInstance(Integer.toString(storeItem.getId()),storeItem.getImageUrl(),storeItem.getTotalCards(),storeItem.getName()),"Vendorfrag").addToBackStack("VendorFrag").commit();
    }

    @Override
    public void buttonClick(StoreItem storeItem) {

        Intent intent = new Intent(getContext(),WebViewActivity.class);
        intent.putExtra("WEBSITE_ADDRESS",storeItem.getUrl());
        intent.putExtra("NAME",storeItem.getName());
        startActivity(intent);
    }

    @Override
    public void buttonClick(StoreItemByCategory storeItemByCategory) {

        Intent intent = new Intent(getContext(),WebViewActivity.class);
        intent.putExtra("WEBSITE_ADDRESS",storeItemByCategory.getUrl());
        intent.putExtra("NAME",storeItemByCategory.getName());

        startActivity(intent);

    }


    public class myTimerTask extends TimerTask {
        @Override
        public void run() {

            HomeFragment.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (viewPager.getCurrentItem() == 0) {
                        viewPager.setCurrentItem(1);
                    } else if (viewPager.getCurrentItem() == 1) {
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }

                }
            });
        }

    }
    private void runOnUiThread(Runnable runnable) {
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
