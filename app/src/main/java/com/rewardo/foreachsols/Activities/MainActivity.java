package com.rewardo.foreachsols.Activities;

import android.net.Uri;
import android.nfc.Tag;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.rewardo.foreachsols.Fragments.CategoriesFragment;
import com.rewardo.foreachsols.Fragments.HomeFragment;
import com.rewardo.foreachsols.Fragments.OffersFragment;
import com.rewardo.foreachsols.Fragments.VendorFragment;
import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.R;
import com.rewardo.foreachsols.Utility.Utility;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener,OffersFragment.OnFragmentInteractionListener,CategoriesFragment.OnFragmentInteractionListener,ToolbarInterface,VendorFragment.OnFragmentInteractionListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    public BottomNavigationView bottomNavigationView;
    public int currentItemId = R.id.storesIcon;
    public int previousItemId = R.id.storesIcon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);

        homeFragment();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(currentItemId == menuItem.getItemId()){

                    return false;
                }
                previousItemId = currentItemId;
                currentItemId = menuItem.getItemId();

                switch (menuItem.getItemId()) {
                    case R.id.storesIcon:

                            homeFragment();
                       // Toast.makeText(MainActivity.this, "StoresIcon clicked..!", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.offersIcon:

                            offersFragment();

                        //Toast.makeText(MainActivity.this, "OffersIcon Clicked..", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.userIcon:

                        Toast.makeText(MainActivity.this, "UserIcon Clicked..", Toast.LENGTH_SHORT).show();
                        break;

                }
                bottomNavigationView.setSelectedItemId(menuItem.getItemId());
                return true;

            }
        });

    }

    public void homeFragment() {
        //adding HomeFragment to MainActivity
        getSupportFragmentManager().beginTransaction().replace(R.id.frameId, HomeFragment.newInstance(null, null),"HomeFrag").commit();
        bottomNavigationView.setSelectedItemId(R.id.storesIcon);

    }

    public void offersFragment() {
        //adding OffersFragment to MainActivity

        getSupportFragmentManager().beginTransaction().replace(R.id.frameId,OffersFragment.newInstance(null,null),"OffersFrag").commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void getToolbarResources(String title, int visibility) {
        getSupportActionBar().setTitle(title);
    }

//    @Override
//    public void onBackStackChanged() {
//
//        int length = getSupportFragmentManager().getBackStackEntryCount();
//
//        StringBuilder msg = new StringBuilder("");
//
//        for (int i = length - 1; i >= 0; i--) {
//
//            msg.append("Index ").append(i).append(" : ");
//            msg.append(getSupportFragmentManager().getBackStackEntryAt(i).getName());
//            msg.append(" \n");
//        }
//
//        Log.i(TAG, "\n" + msg.toString() + " \n ");
//    }

    @Override
    public void onBackPressed() {

        if (bottomNavigationView.getSelectedItemId() != R.id.storesIcon) {
            homeFragment();
            currentItemId = R.id.storesIcon;
            previousItemId = R.id.storesIcon;
        } else {
            super.onBackPressed();
                finish();
        }
    }
}
