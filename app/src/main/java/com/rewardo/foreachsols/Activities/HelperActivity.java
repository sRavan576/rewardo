package com.rewardo.foreachsols.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toolbar;

import com.rewardo.foreachsols.Constants.Constants;
import com.rewardo.foreachsols.Fragments.CategoriesFragment;
import com.rewardo.foreachsols.Fragments.VendorFragment;
import com.rewardo.foreachsols.Interfaces.ICategoriesAdapter;
import com.rewardo.foreachsols.Interfaces.IStoreCardsAdapter;
import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.StoreCards;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.R;

import java.util.ArrayList;

public class HelperActivity extends AppCompatActivity implements CategoriesFragment.OnFragmentInteractionListener,VendorFragment.OnFragmentInteractionListener,ToolbarInterface{

    android.support.v7.widget.Toolbar toolbar;
    ToolbarInterface toolbarCallback;
    TextView title;
    public ArrayList<StoreItem> storeList = new ArrayList<StoreItem>();
    public ArrayList<CategoryItem> categoriesList = new ArrayList<CategoryItem>();
    public StoreItem storeItem = new StoreItem();
    public IStoreCardsAdapter iStoreCardsAdapter;
    public ICategoriesAdapter iCategoriesAdapter;
    public StoreCards storeCards = new StoreCards();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helper);

        toolbarCallback = this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.leftarrow);
        title = findViewById(R.id.tvTitle);
        setUpFragment();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        menu.findItem(R.id.rewardsaction).setVisible(false);
        menu.findItem(R.id.notificationaction).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
             default:
                    return super.onOptionsItemSelected(item);
        }

    }

    private void setUpFragment() {

        int fragmentKey=getIntent().getIntExtra(Constants.FRAGMENT_KEY,0);
        Fragment fragment =  null;
        switch (fragmentKey)
        {
            case 1:

                fragment= VendorFragment.newInstance(getIntent().getStringExtra("ITEMID"),getIntent().getStringExtra("IMAGEURL"),getIntent().getStringExtra("TOTALCARDS"),getIntent().getStringExtra("NAME"));
                title.setText(getIntent().getStringExtra("NAME"));
                break;

            case  2:
                fragment= CategoriesFragment.newInstance(getIntent().getStringExtra("ITEMID"),getIntent().getStringExtra("NAME"));
                title.setText(getIntent().getStringExtra("NAME"));


        }

        if (fragment != null){

            getSupportFragmentManager().beginTransaction().replace(R.id.helperFrame,fragment).commit();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void getToolbarResources(String title, int visibility) {
        getSupportActionBar().setTitle(title);

    }
}
