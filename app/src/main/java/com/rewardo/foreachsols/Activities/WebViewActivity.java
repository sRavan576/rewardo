package com.rewardo.foreachsols.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import com.rewardo.foreachsols.Interfaces.ToolbarInterface;
import com.rewardo.foreachsols.R;

public class WebViewActivity extends AppCompatActivity {

    TextView progressBarText;
    WebView webView;
    Intent intent;
    ProgressBar progressBar;
    TextView title;
    android.support.v7.widget.Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        String url  = getIntent().getStringExtra("WEBSITE_ADDRESS");
        if (url == null || url.isEmpty()) finish();
        setContentView(R.layout.activity_web_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBarText =(TextView)findViewById(R.id.progressBarText);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){

            @Override public void onPageStarted (WebView view, String url, Bitmap favicon){
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                progressBarText.setVisibility(View.VISIBLE);
            }
            @Override public void onPageFinished (WebView view, String url){
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.GONE);
//
            }
        });

        webView.loadUrl(url);




        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.close);
        title = findViewById(R.id.tvTitle);
        title.setText(getIntent().getStringExtra("NAME"));



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
            finish();
        }
    }
}
