package com.rewardo.foreachsols.Constants;

public class Constants {

    public static final String OFFERS_BASE_URL="http://onlineaffiliate.azurewebsites.net/";


    public static final String ICONS_BASE_URL="https://s3.ap-south-1.amazonaws.com/img.storesnoffers.com";

    public static final String FRAGMENT_KEY = "fragment_key";
}
