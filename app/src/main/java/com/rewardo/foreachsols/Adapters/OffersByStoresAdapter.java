package com.rewardo.foreachsols.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rewardo.foreachsols.Interfaces.IOffersAdapter;
import com.rewardo.foreachsols.Interfaces.IOffersByStoresAdapter;
import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.OfferItem;
import com.rewardo.foreachsols.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OffersByStoresAdapter extends RecyclerView.Adapter<OffersByStoresAdapter.ViewHolder> {

    public IOffersByStoresAdapter iOffersByStoresAdapter;
    public ArrayList<OfferItem> list;
    private Context context;


    public OffersByStoresAdapter(ArrayList<OfferItem> list, Context context, IOffersByStoresAdapter iOffersByStoresAdapter) {
        this.list = list;
        this.context = context;
        this.iOffersByStoresAdapter = iOffersByStoresAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.offerslist, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull OffersByStoresAdapter.ViewHolder viewHolder, int position) {

        OfferItem item = list.get(position);


        viewHolder.tvVendorName.setText(item.getVendorName());
        viewHolder.tvEndStatus.setText(item.getEndsIn());
        viewHolder.tvOfferId.setText(item.getId().toString());
        Picasso.get().load(item.getVendorImageUrl()).into(viewHolder.ivVendorLogo);

        if (item.getImageUrl() == null) {
            viewHolder.tvTitle.setVisibility(View.VISIBLE);
            viewHolder.tvTitle.setText(item.getTitle());
        } else {
            viewHolder.tvTitle.setVisibility(View.GONE);
            viewHolder.ivImage.setVisibility(View.VISIBLE);
            Picasso.get().load(item.getImageUrl()).into(viewHolder.ivImage);
        }

        if (item.getHotDealScale() == 51) {
            Picasso.get().load("http://www.clove7.com/images/offersicon.png").into(viewHolder.ivDeal);
        } else if (item.getHotDealScale() == 52) {
            Picasso.get().load("https://images.vexels.com/media/users/3/127148/isolated/preview/f5d0ce2548bacacd7cfefa9c713d3f21-etiqueta-origami-oferta-big-by-vexels.png").into(viewHolder.ivDeal);
        } else {
            Picasso.get().load("http://ck.lnwfile.com/_/ck/_raw/25/gb/45.png").into(viewHolder.ivDeal);
        }


        Picasso.get().load(item.getVendorImageUrl()).into(viewHolder.ivVendorLogo);

        viewHolder.cvOfferCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iOffersByStoresAdapter.cardClick(item);
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        public TextView tvVendorName;
        public TextView tvEndStatus;
        public TextView tvOfferId;
        public TextView tvTitle;
        public ImageView ivVendorLogo;
        public ImageView ivDeal;
        public ImageView ivImage;
        public CardView cvOfferCards;




        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvVendorName = (TextView) itemView.findViewById(R.id.offersvendorName);
            tvEndStatus = (TextView) itemView.findViewById(R.id.offersEndStatus);
            tvOfferId = (TextView) itemView.findViewById(R.id.offersId);
            tvTitle = (TextView) itemView.findViewById(R.id.offersTitle);
            ivVendorLogo = (ImageView) itemView.findViewById(R.id.offersVendorLogo);
            ivDeal = (ImageView) itemView.findViewById(R.id.offersDeal);
            ivImage = (ImageView) itemView.findViewById(R.id.offersImage);
            cvOfferCards=(CardView)itemView.findViewById(R.id.offerCards);
        }
    }
}

