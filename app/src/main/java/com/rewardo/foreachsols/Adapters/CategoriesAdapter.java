package com.rewardo.foreachsols.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.rewardo.foreachsols.Interfaces.ICategoriesAdapter;
import com.rewardo.foreachsols.Interfaces.IStoreCardsAdapter;
import com.rewardo.foreachsols.Interfaces.IStoresByCategoryAdapter;
import com.rewardo.foreachsols.Models.Categories;
import com.rewardo.foreachsols.Models.CategoryItem;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.Models.StoreItemByCategory;
import com.rewardo.foreachsols.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.rewardo.foreachsols.Constants.Constants.ICONS_BASE_URL;

public class CategoriesAdapter extends  RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    public ICategoriesAdapter iCategoriesAdapter;
    public ArrayList<CategoryItem> list;
    private Context context;

    public CategoriesAdapter(ArrayList<CategoryItem> list, Context context, ICategoriesAdapter iCategoriesAdapter) {
        this.list = list;
        this.context = context;
        this.iCategoriesAdapter = iCategoriesAdapter;
    }

    @NonNull
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.categorieslist,parent,false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,  int position) {

        CategoryItem  categoryItem = list.get(position);

       Picasso.get().load(new StringBuilder().append(ICONS_BASE_URL).append(categoryItem.getIconUrl()).toString()).into(viewHolder.ib_categoryIcon);
        viewHolder.tv_categoryName.setText(categoryItem.getName());

        viewHolder.cvCategoryCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                iCategoriesAdapter.itemClick(categoryItem,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ib_categoryIcon;
        public TextView tv_categoryName;
        public CardView cvCategoryCards;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ib_categoryIcon=(ImageView) itemView.findViewById(R.id.categoryIcon);
            tv_categoryName=(TextView)itemView.findViewById(R.id.categoryName);
            cvCategoryCards=(CardView) itemView.findViewById(R.id.categoryCard);

        }
    }
}
