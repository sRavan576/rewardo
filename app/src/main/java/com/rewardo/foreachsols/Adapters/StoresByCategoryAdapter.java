package com.rewardo.foreachsols.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rewardo.foreachsols.Interfaces.IStoresByCategoryAdapter;
import com.rewardo.foreachsols.Models.StoreItemByCategory;
import com.rewardo.foreachsols.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StoresByCategoryAdapter extends RecyclerView.Adapter<StoresByCategoryAdapter.ViewHolder>{


    public IStoresByCategoryAdapter iStoresByCategoryAdapter;
    public ArrayList<StoreItemByCategory> list;
    private Context context;

    public StoresByCategoryAdapter(ArrayList<StoreItemByCategory> list, Context context, IStoresByCategoryAdapter iStoresByCategoryAdapter) {
        this.list = list;
        this.context = context;
        this.iStoresByCategoryAdapter = iStoresByCategoryAdapter;
    }

    @NonNull
    @Override
    public StoresByCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.storeslist,parent,false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull StoresByCategoryAdapter.ViewHolder viewHolder, int position) {

        StoreItemByCategory  storeItemByCategory = list.get(position);

        viewHolder.tv_vendorName.setText(storeItemByCategory.getName());
        viewHolder.tv_totalCards.setText(storeItemByCategory.getTotalCards());
        Picasso.get().load(storeItemByCategory.getImageUrl()).into(viewHolder.iv_vendorLogo);
        Picasso.get().load(R.drawable.rewardicon).into(viewHolder.iv_rewardsIcon);

        viewHolder.bt_shopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iStoresByCategoryAdapter.buttonClick(storeItemByCategory);
//                StoreItemByCategory storeItemByCategory = list.get(position);
//                String url = storeItemByCategory.getUrl();
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(i);
            }
        });






    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView iv_vendorLogo;
        public TextView tv_vendorName;
        public ImageView iv_rewardsIcon;
        public TextView tv_totalCards;
        public Button bt_shopButton;
      //  public CardView cv_groceryCards;


        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            iv_vendorLogo=(ImageView) itemView.findViewById(R.id.vendorLogo);
            tv_vendorName=(TextView)itemView.findViewById(R.id.vendorName);
            iv_rewardsIcon=(ImageView)itemView.findViewById(R.id.rewardsIcon);
            tv_totalCards=(TextView)itemView.findViewById(R.id.totalCards);
            bt_shopButton=(Button)itemView.findViewById(R.id.shopButton);
           // cv_groceryCards=(CardView)itemView.findViewById(R.id.groceryCards);


        }
    }
}
