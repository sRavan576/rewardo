package com.rewardo.foreachsols.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rewardo.foreachsols.Interfaces.IOffersAdapter;
import com.rewardo.foreachsols.Models.OfferItem;
import com.rewardo.foreachsols.Models.Offers;
import com.rewardo.foreachsols.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {

    public  IOffersAdapter iOffersAdapter;
    public ArrayList<OfferItem> list;
    private Context context;

    public OffersAdapter(ArrayList<OfferItem> list, Context context, IOffersAdapter iOffersAdapter) {
        this.list = list;
        this.context = context;
        this.iOffersAdapter = iOffersAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.offerslist,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        OfferItem item = list.get(position);

        holder.tvVendorName.setText(item.getVendorName());
        holder.tvEndStatus.setText(item.getEndsIn());
        holder.tvOfferId.setText(item.getId().toString());
        Picasso.get().load(item.getVendorImageUrl()).into(holder.ivVendorLogo);

        if(item.getImageUrl() == null){
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(item.getTitle());
        }
        else{
            holder.tvTitle.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.VISIBLE);
            Picasso.get().load(item.getImageUrl()).into(holder.ivImage);
        }

        if (item.getHotDealScale() == 51)
        {
            Picasso.get().load("http://www.clove7.com/images/offersicon.png").into(holder.ivDeal);
        }

        else if (item.getHotDealScale()==52)
        {
            Picasso.get().load("https://images.vexels.com/media/users/3/127148/isolated/preview/f5d0ce2548bacacd7cfefa9c713d3f21-etiqueta-origami-oferta-big-by-vexels.png").into(holder.ivDeal);
        }

        else
        {
            Picasso.get().load("http://ck.lnwfile.com/_/ck/_raw/25/gb/45.png").into(holder.ivDeal);
        }



        Picasso.get().load(item.getVendorImageUrl()).into(holder.ivVendorLogo);

        holder.cvOfferCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iOffersAdapter.cardClick(item);
//                OfferItem item = list.get(position);
//                String url = item.getOfferUrl();
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateItems(ArrayList<OfferItem> items){
        list.addAll(items);
        //notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvVendorName;
        public TextView tvEndStatus;
        public TextView tvOfferId;
        public TextView tvTitle;
        public ImageView ivVendorLogo;
        public ImageView ivDeal;
        public ImageView ivImage;
        public CardView cvOfferCards;

        public ViewHolder(View view){
            super(view);

            tvVendorName = (TextView) view.findViewById(R.id.offersvendorName);
            tvEndStatus = (TextView) view.findViewById(R.id.offersEndStatus);
            tvOfferId = (TextView) view.findViewById(R.id.offersId);
            tvTitle = (TextView) view.findViewById(R.id.offersTitle);
            ivVendorLogo = (ImageView) view.findViewById(R.id.offersVendorLogo);
            ivDeal = (ImageView) view.findViewById(R.id.offersDeal);
            ivImage = (ImageView) view.findViewById(R.id.offersImage);
            cvOfferCards = (CardView) view.findViewById(R.id.offerCards);
        }
    }
}
