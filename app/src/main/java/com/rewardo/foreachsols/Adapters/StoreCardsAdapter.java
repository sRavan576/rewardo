package com.rewardo.foreachsols.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rewardo.foreachsols.Interfaces.IStoreCardsAdapter;
import com.rewardo.foreachsols.Models.StoreItem;
import com.rewardo.foreachsols.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StoreCardsAdapter extends RecyclerView.Adapter<StoreCardsAdapter.ViewHolder> {

    public IStoreCardsAdapter iStoreCardsAdapter;
    public ArrayList<StoreItem> list;
    private Context context;

    public StoreCardsAdapter(ArrayList<StoreItem> list, Context context, IStoreCardsAdapter iStoreCardsAdapter) {
        this.list = list;
        this.context = context;
        this.iStoreCardsAdapter = iStoreCardsAdapter;
    }

    @NonNull
    @Override
    public StoreCardsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.storeslist,parent,false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull StoreCardsAdapter.ViewHolder viewHolder, int position) {

        StoreItem  storeItem = list.get(position);

        viewHolder.tv_vendorName.setText(storeItem.getName());
        viewHolder.tv_totalCards.setText(storeItem.getTotalCards());
        Picasso.get().load(storeItem.getImageUrl()).into(viewHolder.iv_vendorLogo);
        Picasso.get().load(R.drawable.rewardicon).into(viewHolder.iv_rewardsIcon);

        viewHolder.bt_shopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    iStoreCardsAdapter.buttonClick(storeItem);


//                StoreItem item = list.get(position);
//                String url = item.getUrl();
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(i);
            }
        });

        viewHolder.cv_storeCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                iStoreCardsAdapter.itemClick(storeItem,position);
            }
        });




    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView iv_vendorLogo;
        public TextView tv_vendorName;
        public ImageView iv_rewardsIcon;
        public TextView tv_totalCards;
        public Button bt_shopButton;
        public CardView cv_storeCards;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            iv_vendorLogo=(ImageView) itemView.findViewById(R.id.vendorLogo);
            tv_vendorName=(TextView)itemView.findViewById(R.id.vendorName);
            iv_rewardsIcon=(ImageView)itemView.findViewById(R.id.rewardsIcon);
            tv_totalCards=(TextView)itemView.findViewById(R.id.totalCards);
            bt_shopButton=(Button)itemView.findViewById(R.id.shopButton);
            cv_storeCards =(CardView)itemView.findViewById(R.id.storeCards);


        }
    }
}
